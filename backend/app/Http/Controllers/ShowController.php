<?php

namespace App\Http\Controllers;

use Cron\FieldFactory;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ShowController extends Controller
{

    public function getShows (Request $request) {
        $search = $request->input('q');

        // if no search string is set return default data
        if (empty($search)){
            return Http::get(env('API_ROOT').'/shows')->json();
        }

        $response = Http::get(env('API_ROOT').'/search/shows?q='.$search);
        return $this->filterByName($response->json(), $search);

    }

    private function filterByName (array $list, string $name): array {

        if (empty($list)){
            return [];
        }

        $filteredList = [];
        foreach ($list as $item) {
            if ($item['show']['name'] === $name){
                $filteredList[] = $item;
            }
        }

        return [$filteredList[0]['show']];

    }

}
