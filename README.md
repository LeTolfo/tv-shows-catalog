# TV Shows catalog

The challenge is to create a Frontend application connected to the JSON API, which will allow the consumer to browse catalog of all available TV shows and perform search for TV shows by their name.

#install

Clone project

## backend
```angular2html
cd backend
npm install
```

add the following to the backend/**.env** file 

```angular2html
API_ROOT=http://api.tvmaze.com
```

ser the backend api

```angular2html
php artisan serve --port=8000
```

## frontend

### Project setup
```
cd frontend
npm install
```
### Set api link

update if necessary the backend api `url` on the `/frontend/src/store.js` file

### Compiles and hot-reloads for development
```
npm run serve
```
