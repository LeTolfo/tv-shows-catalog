import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    listType: 'grid',
    search: '',
    api: 'http://127.0.0.1:8000',
    shows: [],

  },
  mutations: {
    setListType: (state, type) => state.listType = type,
    setSearch: (state, search) => state.search = search,
    setShows: (state, shows) => state.shows = shows,

  }
});

export default store
