import Vue from 'vue'
import Router from 'vue-router'

// import views
import Shows from '@/components/Shows';

Vue.use(Router)

const router = new Router(
  {
    mode: 'history',
    routes: [
      {
        path: '/',
        component: Shows
      },
    ],
  }
)

export default router;
